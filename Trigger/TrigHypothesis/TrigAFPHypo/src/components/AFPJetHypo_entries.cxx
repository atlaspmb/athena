#include "../TrigAFPDijetComboHypoTool.h"
#include "../AFPProtonTransportTool.h"
#include "../TestTrigAFPDijetHypoAlg.h"
#include "../TestTrigAFPDijetHypoTool.h"

DECLARE_COMPONENT( TestTrigAFPDijetHypoAlg )
DECLARE_COMPONENT( TestTrigAFPDijetHypoTool )
DECLARE_COMPONENT( TrigAFPDijetComboHypoTool )
DECLARE_COMPONENT( AFPProtonTransportTool )
